﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour {
	
	public float MaxX = 0.0f;
	public float MaxY = 0.0f;
	public Vector3 startPos;
	public EnemyMove enemyPrefab;
	// Use this for initialization
	void Start () {
//		EnemyMove emeny = Instantiate (enemyPrefab);
//		float start = Random.Range (1, 20);
//		startPos.Set (start, 0, 0);
//		transform.position = startPos;
//		float edge = Random.value * 100;
//		Debug.Log (edge);
//		if (edge < 33) {
//			Debug.Log ("LEFT");
//			MaxX = 0.17f;
//			float LeftY = Random.Range (0, 10);
//			newPos.Set (MaxX, LeftY, 0);
//		}
//		if (edge > 67) {
//			Debug.Log ("RIGHT");
//			MaxX = 22.0f;
//			float RightY = Random.Range (0, 10);
//			newPos.Set (MaxX, RightY, 0);
//		}
//		if (edge > 34 && edge < 66) {
//			Debug.Log ("TOP");
//			MaxY =10;
//			float TopY = Random.Range (0, 22);
//			newPos.Set (TopY, MaxY, 0);
//		}
//
	}
	float speed = 10.0f;
	public Transform target;
	public Vector3 newPos;
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonUp("Fire1")) {
			EnemyMove emeny = Instantiate (enemyPrefab);
			float start = Random.Range (1, 20);
			startPos.Set (start, 0, 0);
			transform.position = startPos;
			float edge = Random.value * 100;
			Debug.Log (edge);
			if (edge < 33) {
				Debug.Log ("LEFT");
				MaxX = 0.17f;
				float LeftY = Random.Range (0, 10);
				newPos.Set (MaxX, LeftY, 0);
			}
			if (edge > 67) {
				Debug.Log ("RIGHT");
				MaxX = 22.0f;
				float RightY = Random.Range (0, 10);
				newPos.Set (MaxX, RightY, 0);
			}
			if (edge > 34 && edge < 66) {
				Debug.Log ("TOP");
				MaxY =10;
				float TopY = Random.Range (0, 22);
				newPos.Set (TopY, MaxY, 0);
			}

		}
		Vector2 direction = newPos - transform.position;
		direction = direction.normalized;

		Vector2 velocity = direction * speed;
		transform.Translate (velocity * Time.deltaTime);

	}
	void OnDrawGizmos(){
		//Gizmos.color = Color.red;
		//Gizmos.DrawRay (transform.position, heading);

		Gizmos.color = Color.red;
		Vector2 direction = newPos - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}
}
